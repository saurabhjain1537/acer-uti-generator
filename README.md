# acer-uti-generator

## Description
acer-uti-generator is a utility library to generate UTI (Unique Transaction Identifier) following the guidance provided by ACER (European Union Agency for the Cooperation of Energy Regulators) in TRUM (Transaction Reporting User Manual).

Please refer to the following link for details:
https://www.acer.europa.eu/remit-documents/remit-reporting-guidance

## Development Setup
  - Java 17 https://adoptium.net/temurin/releases/?os=windows&arch=x64&package=jdk&version=17
  - Maven https://archive.apache.org/dist/maven/maven-3/3.9.5/binaries/
  - Eclipse https://www.eclipse.org/downloads/packages/
    - Sonar Lint
	 - EclEmma JaCoCo

## Installation
Use the following command to compile the code

`mvn clean install dependency:copy-dependencies -DincludeScope=runtime`

This generates the following -

  - acer-uti-generator jar under the directory *target\*
  - dependency jars under the directory *target\dependency*

The application using acer-uti-generator should have the above mentioned jar file in the java classpath.
Optionally *uti-generator.properties* can be added to classpath.

## Configuration
  *uti-generator.properties* supports following config properties -
  - DBFilepath
    - default value = acer-uti-gen.db 
    - The utility uses sqlite to keep track of generated UTIs.
    - It is used to generate progressive number for equal trades on the same date.
  - NormalizerType
    - default value = Normalizer 
    - valid values = Normalizer, AcerNormalizer
    - **Normalizer** means the implementation of ACER Annex IV - Guidance on the Unique Transaction ID (UTI) v2.2.
    - **AcerNormalizer** means the that generated UTIs exactly match the Generator excel provided by ACER.
    - Ideally ACER guide and generator should have same logic. But Generator Excel always has the quantity as 1.0 in the concatenated string.
    - UTI generated by Excel is different from the algo. 

## Usage

For example, create following directories:
 - uti-client
     - lib

The directory uti-client\lib contains following files:
  - acer-uti-generator-1.0.1.jar
  - commons-codec-1.15.jar
  - logback-classic-1.2.3.jar
  - logback-core-1.2.3.jar
  - slf4j-api-1.7.25.jar
  - sqlite-jdbc-3.27.2.1.jar
  - uti-generator.properties

Create a Java class, say ReportingApp.java to invoke acer-uti-generator.
Change Dir to uti-client
Execute the following command

`java --class-path "lib\*;lib\" ReportingApp.java`

## Support
Please contact via email: support.earlobe937@passmail.net

## Pending Enhancements
- Data Purging - deletion of old records saved in sqlite DB Table uti_hash after 60 days