package reporting.utils.uti;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ConfigurationTest {

  @Test
  @DisplayName("Invalid Properties file")
  void missingPropertiesFileTest() {
    
    Configuration config = Configuration.getInstance("does-not-exists.properties");
    Assertions.assertEquals("None", config.getProperty("DBFilepath", "None"));
  }

}
