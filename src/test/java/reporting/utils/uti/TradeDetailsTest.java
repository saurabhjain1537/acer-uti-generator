package reporting.utils.uti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import reporting.utils.enums.Commodity;
import reporting.utils.enums.ContractType;
import reporting.utils.enums.SettlementMethod;
import reporting.utils.uti.TradeDetails.TradeDetailsBuilder;

@DisplayName("TradeDetails Validation")
public class TradeDetailsTest {

  private TradeDetailsBuilder tradeBuilder;

  @BeforeEach
  void setup() {
    tradeBuilder = new TradeDetailsBuilder();
    //@formatter:off
    tradeBuilder.buyerCode("Company001")
        .sellerCode("Company001")
        .contractType(ContractType.FORWARD)
        .commodity(Commodity.NATURAL_GAS)
        .settlement(SettlementMethod.PHYSICAL)
        .tradeDate("2024-04-01")
        .deliveryPoint("10YCB-EUROPEU--8")
        .startDate("2025-01-01")
        .endDate("2025-12-31");
    //@formatter:on
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = {" ", ""})
  @DisplayName("BuyerCode Missing")
  void buyerCode(String buyerCode) {
    IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
      tradeBuilder.buyerCode(buyerCode).build();
    }, "IllegalArgumentException was expected");
    assertEquals("buyerCode is missing.", thrown.getMessage());
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = {" ", ""})
  @DisplayName("SellerCode Missing")
  void sellerCode(String sellerCode) {
    IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
      tradeBuilder.sellerCode(sellerCode).build();
    }, "IllegalArgumentException was expected");
    assertEquals("sellerCode is missing.", thrown.getMessage());
  }

  @Test
  @DisplayName("ContractType Missing")
  void contractType() {
    IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
      tradeBuilder.contractType(null).build();
    }, "IllegalArgumentException was expected");
    assertEquals("contractType is missing.", thrown.getMessage());
  }

  @Test
  @DisplayName("Commodity Missing")
  void commodity() {
    IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
      tradeBuilder.commodity(null).build();
    }, "IllegalArgumentException was expected");
    assertEquals("commodity is missing.", thrown.getMessage());
  }

  @Test
  @DisplayName("Settlement Missing")
  void settlement() {
    IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
      tradeBuilder.settlement(null).build();
    }, "IllegalArgumentException was expected");
    assertEquals("settlement is missing.", thrown.getMessage());
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = {" ", ""})
  @DisplayName("TradeDate Missing")
  void tradeDate(String tradeDate) {
    IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
      tradeBuilder.tradeDate(tradeDate).build();
    }, "IllegalArgumentException was expected");
    assertEquals("tradeDate is missing.", thrown.getMessage());
  }

  @ParameterizedTest
  @ValueSource(strings = {"abc", "15-08-2024"})
  @DisplayName("TradeDate Invalid")
  void tradeDateInvalid(String tradeDate) {
    IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
      tradeBuilder.tradeDate(tradeDate).build();
    }, "IllegalArgumentException was expected");
    assertEquals("tradeDate is invalid. Valid format: YYYY-MM-DD.", thrown.getMessage());
  }

}
