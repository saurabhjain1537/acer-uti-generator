package reporting.utils.uti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Test UtiHashDao")
class UtiHashDaoTest {

  private String dbFilePath;

  @BeforeEach
  void setup() {
    
    dbFilePath = Configuration.getInstance().getProperty("DBFilepath", "acer-uti-gen.db");
  }
  
  @AfterEach
  void cleanUp() {
    try {
      Files.deleteIfExists(Paths.get(dbFilePath));
      Files.deleteIfExists(Paths.get("acer-uti-gen.db"));
    } catch (IOException e) {
      e.printStackTrace();
    }    
  }
  
  @Test
  @DisplayName("Increment Once")
  void testIncrementOnce() {
    int progressNumber = 0;
    try (var hashDao = new UtiHashDao(dbFilePath)) {
      progressNumber = hashDao.incrementCount("abcd1");
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertEquals(1, progressNumber);
  }

  @Test
  @DisplayName("Increment Twice")
  void testIncrementTwice() {
    int progressNumber = 0;
    try (var hashDao = new UtiHashDao(dbFilePath)) {
      progressNumber = hashDao.incrementCount("abcd2");
      progressNumber = hashDao.incrementCount("abcd2");
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertEquals(2, progressNumber);
  }
  
  @Test
  @DisplayName("Null Hash")
  void testNullHash() {
    int progressNumber = 0;
    try (var hashDao = new UtiHashDao()) {
      progressNumber = hashDao.incrementCount(null);
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertEquals(0, progressNumber);
  }


  
}
