package reporting.utils.uti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.junit.jupiter.params.provider.ValueSource;
import reporting.utils.enums.Commodity;
import reporting.utils.enums.ContractType;
import reporting.utils.enums.Currency;
import reporting.utils.enums.QuantityUnit;
import reporting.utils.enums.SettlementMethod;
import reporting.utils.uti.TradeDetails.TradeDetailsBuilder;

@DisplayName("Normalize")
class NormalizerTest {

  private TradeDetailsBuilder tradeBuilder;

  @BeforeEach
  void setup() {
    tradeBuilder = new TradeDetailsBuilder();
    //@formatter:off
    tradeBuilder.buyerCode("Company001")
                .sellerCode("Company001")
                .contractType(ContractType.FORWARD)
                .commodity(Commodity.NATURAL_GAS)
                .settlement(SettlementMethod.PHYSICAL)
                .tradeDate("2024-04-01")
                .deliveryPoint("10YCB-EUROPEU--8")
                .startDate("2025-01-01")
                .endDate("2025-12-31");
    //@formatter:on
  }

  @Nested
  @DisplayName("Settlement Method")
  class SettlementMethodTest {

    @Test
    @DisplayName("Physical")
    void testPhysical() {
      TradeDetails trade = tradeBuilder.build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(SettlementMethod.PHYSICAL.getSettlementName(), normalizer.getSettlementMethod());
    }

    @Test
    @DisplayName("Cash")
    void testCash() {
      TradeDetails trade = tradeBuilder.settlement(SettlementMethod.CASH).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(SettlementMethod.CASH.getSettlementName(), normalizer.getSettlementMethod());
    }

    @Test
    @DisplayName("Optional normalizes to Physical")
    void testOptional() {
      TradeDetails trade = tradeBuilder.settlement(SettlementMethod.OPTIONAL).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(SettlementMethod.PHYSICAL.getSettlementName(), normalizer.getSettlementMethod());
    }
  }
  
  @Nested
  @DisplayName("Contract Type")
  class ContractTypeTest {

    @Test
    @DisplayName("Forward")
    void testPhysical() {
      TradeDetails trade = tradeBuilder.build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(ContractType.FORWARD.getContractTypeValue(), normalizer.getContractType());
    }

    @ParameterizedTest
    @EnumSource(mode = Mode.MATCH_ALL, names = "^OPTION.*$")
    @DisplayName("Option")
    void testOption(ContractType inputContractType) {
      TradeDetails trade = tradeBuilder.contractType(inputContractType).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(ContractType.OPTION.getContractTypeValue(), normalizer.getContractType());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"PHYSICAL", "OPTIONAL"})
    @DisplayName("Swap with Physical Settlement")
    void testSwap(SettlementMethod inputSettlementMethod) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.contractType(ContractType.SWAP)
                                       .settlement(inputSettlementMethod)
                                       .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(ContractType.FORWARD.getContractTypeValue(), normalizer.getContractType());
    }

    @ParameterizedTest
    @EnumSource(names = {"PHYSICAL", "OPTIONAL"})
    @DisplayName("Spread with Physical Settlement")
    void testSpread(SettlementMethod inputSettlementMethod) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.contractType(ContractType.SPREAD)
          .settlement(inputSettlementMethod)
          .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(ContractType.FORWARD.getContractTypeValue(), normalizer.getContractType());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"SWAP", "SPREAD"})
    @DisplayName("Swap, Spread with Cash Settlement")
    void testSpread(ContractType inputContractType) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.contractType(inputContractType)
          .settlement(SettlementMethod.CASH)
          .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(ContractType.SWAP.getContractTypeValue(), normalizer.getContractType());
    }

    @ParameterizedTest
    @EnumSource(names = {"CASH", "OPTIONAL"})
    @DisplayName("Swing")
    void testSwing(SettlementMethod inputSettlementMethod) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.contractType(ContractType.SWING)
          .settlement(inputSettlementMethod)
          .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(ContractType.SWING.getContractTypeValue(), normalizer.getContractType());
    }
    
    @Test
    @DisplayName("Swing with Physical Settlement")
    void testSwing() {
      //@formatter:off
      TradeDetails trade = tradeBuilder.contractType(ContractType.SWING)
          .settlement(SettlementMethod.PHYSICAL)
          .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(ContractType.FORWARD.getContractTypeValue(), normalizer.getContractType());
    }
  }

  @Nested
  @DisplayName("Price Currency")
  class PriceCurrencyTest {

    @Test
    @DisplayName("EUX")
    void testEUR() {
      TradeDetails trade = tradeBuilder.priceCurrency(Currency.EUX).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(Currency.EUR.name(), normalizer.getPriceCurrency());
    }

    @Test
    @DisplayName("GBX")
    void testGBX() {
      TradeDetails trade = tradeBuilder.priceCurrency(Currency.GBX).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(Currency.GBP.name(), normalizer.getPriceCurrency());
    }
    
    @ParameterizedTest
    @EnumSource(mode = Mode.EXCLUDE, names = {"EUX", "GBX"})
    @DisplayName("Major Currencies")
    void testOption(Currency inputPriceCurrency) {
      TradeDetails trade = tradeBuilder.priceCurrency(inputPriceCurrency).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals(inputPriceCurrency.name(), normalizer.getPriceCurrency());
    }
    
  }
  
  @Nested
  @DisplayName("Price")
  class PriceTest {
    
    @ParameterizedTest
    @ValueSource(doubles = {48.543, 48.543001, 48.542995})
    @DisplayName("Round to 5 decimals")
    void testRounding(double price) {
      TradeDetails trade = tradeBuilder.price(price).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("48.54300", normalizer.getPrice());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"EUX", "GBX"})
    @DisplayName("Minor Currencies with Standard Quantity Unit")
    void testPence(Currency priceCurrency) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.price(748.543)
                                       .priceCurrency(priceCurrency)
                                       .quantityUnit(QuantityUnit.MWH_PER_H)
                                       .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("7.48543", normalizer.getPrice());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"EUX", "GBX"})
    @DisplayName("Minor Currencies with Non-Standard Quantity Unit")
    void testPenceWithNonStandardQuantity(Currency priceCurrency) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.price(748.543)
          .priceCurrency(priceCurrency)
          .quantityUnit(QuantityUnit.KWH_PER_D)
          .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("7485.43000", normalizer.getPrice());
    }

    @ParameterizedTest
    @EnumSource(mode = Mode.EXCLUDE, names = {"EUX", "GBX"})
    @DisplayName("Major Currencies with Standard Quantity Unit")
    void testMajorWithStandardQuantity(Currency priceCurrency) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.price(7.4854325)
                                       .priceCurrency(priceCurrency)
                                       .quantityUnit(QuantityUnit.MWH_PER_H)
                                       .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("7.48543", normalizer.getPrice());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"GW", "GWH_PER_H", "GWH_PER_D", "KTHERM_PER_D", "TCM_PER_D", "GJ_PER_D"})
    @DisplayName("GBP with Non-Standard Quantity Unit 1000's")
    void testNonStandardQuantity(QuantityUnit quantityUnit) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.price(7485.4325)
                                       .priceCurrency(Currency.GBP)
                                       .quantityUnit(quantityUnit)
                                       .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("7.48543", normalizer.getPrice());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"MTHERM_PER_D", "MCM_PER_D", "MMBTU_PER_D", "MMJ_PER_D"})
    @DisplayName("GBP with Non-Standard Quantity Unit Millions")
    void testNonStandardQuantityMillion(QuantityUnit quantityUnit) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.price(7485432.5)
          .priceCurrency(Currency.GBP)
          .quantityUnit(quantityUnit)
          .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("7.48543", normalizer.getPrice());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"KW", "KWH_PER_H", "KWH_PER_D"})
    @DisplayName("GBP with KW Quantity Unit ")
    void testKWQuantity(QuantityUnit quantityUnit) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.price(7.4854325)
                                       .priceCurrency(Currency.GBP)
                                       .quantityUnit(quantityUnit)
                                       .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("7485.43250", normalizer.getPrice());
    }
  }
  
  @Nested
  @DisplayName("Quantity Unit")
  class QuantityUnitTest {

    @Test
    @DisplayName("Null")
    void testNull() {
      TradeDetails trade = tradeBuilder.build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("", normalizer.getQuantityUnit());
    }

    @ParameterizedTest
    @EnumSource(names = {"KW", "KWH_PER_H", "KWH_PER_D", "MW", "MWH_PER_H", "MWH", "MWH_PER_D", "GW", "GWH_PER_H", "GWH_PER_D"})
    @DisplayName("MW")
    void testMW(QuantityUnit inputQuantityUnit) {
      TradeDetails trade = tradeBuilder.quantityUnit(inputQuantityUnit).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("MW", normalizer.getQuantityUnit());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"THERMS_PER_D", "KTHERM_PER_D", "MTHERM_PER_D"})
    @DisplayName("Therm/d")
    void testTherms(QuantityUnit inputQuantityUnit) {
      TradeDetails trade = tradeBuilder.quantityUnit(inputQuantityUnit).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("Therm/d", normalizer.getQuantityUnit());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"CM_PER_D", "MCM_PER_D", "TCM_PER_D"})
    @DisplayName("cm/d")
    void testCM(QuantityUnit inputQuantityUnit) {
      TradeDetails trade = tradeBuilder.quantityUnit(inputQuantityUnit).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("cm/d", normalizer.getQuantityUnit());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"BTU_PER_D", "MMBTU_PER_D"})
    @DisplayName("Btu/d")
    void testBTU(QuantityUnit inputQuantityUnit) {
      TradeDetails trade = tradeBuilder.quantityUnit(inputQuantityUnit).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("Btu/d", normalizer.getQuantityUnit());
    }

    @ParameterizedTest
    @EnumSource(names = {"MJ_PER_D", "MJ_100_PER_D", "MMJ_PER_D", "GJ_PER_D"})
    @DisplayName("MJ/d")
    void testMJ(QuantityUnit inputQuantityUnit) {
      TradeDetails trade = tradeBuilder.quantityUnit(inputQuantityUnit).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("MJ/d", normalizer.getQuantityUnit());
    }
    
  }
  
  @Nested
  @DisplayName("Quantity")
  class QuantityTest {
    
    @ParameterizedTest
    @ValueSource(doubles = {4.54333326, 4.54333326004, 4.54333325995})
    @DisplayName("Round to 10 decimals")
    void testRounding(double quantity) {
      TradeDetails trade = tradeBuilder.quantity(quantity).build();
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("4.5433332600", normalizer.getQuantity());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"GW", "GWH_PER_H", "KTHERM_PER_D", "TCM_PER_D", "GJ_PER_D"})
    @DisplayName("Non-Standard Quantity Unit 1000's")
    void testNonStandardThousandQuantity(QuantityUnit quantityUnit) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.quantity(4.54333326)
                                       .priceCurrency(Currency.GBP)
                                       .quantityUnit(quantityUnit)
                                       .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("4543.3332600000", normalizer.getQuantity());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"MTHERM_PER_D", "MCM_PER_D", "MMBTU_PER_D", "MMJ_PER_D"})
    @DisplayName("Non-Standard Quantity Unit Millions")
    void testNonStandardMillionQuantity(QuantityUnit quantityUnit) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.quantity(0.0454333326)
          .priceCurrency(Currency.GBP)
          .quantityUnit(quantityUnit)
          .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("45433.3326000000", normalizer.getQuantity());
    }
    
    @ParameterizedTest
    @EnumSource(names = {"KWH_PER_H", "KW"})
    @DisplayName("KW")
    void testKW(QuantityUnit quantityUnit) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.quantity(4543.33326)
          .priceCurrency(Currency.GBP)
          .quantityUnit(quantityUnit)
          .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("4.5433332600", normalizer.getQuantity());
    }
    
    @ParameterizedTest
    @CsvSource({
      "KWH_PER_D, 480.0",
      "MWH_PER_D, 0.48",
      "GWH_PER_D, 0.00048"
    })
    @DisplayName("Daily Flow")
    void testDailyFlow(QuantityUnit quantityUnit, double quantity) {
      //@formatter:off
      TradeDetails trade = tradeBuilder.quantity(quantity)
          .priceCurrency(Currency.GBP)
          .quantityUnit(quantityUnit)
          .build();
      //@formatter:on
      Normalizer normalizer = new Normalizer(trade);
      assertEquals("0.0200000000", normalizer.getQuantity());
    }

  }
  
}
