package reporting.utils.uti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import reporting.utils.enums.Commodity;
import reporting.utils.enums.ContractType;
import reporting.utils.enums.Currency;
import reporting.utils.enums.QuantityUnit;
import reporting.utils.enums.SettlementMethod;
import reporting.utils.uti.TradeDetails.TradeDetailsBuilder;

@DisplayName("Generate UTI")
public class UtiGeneratorTest {

  private TradeDetailsBuilder tradeBuilder;

  @AfterEach
  void cleanUp() {
    try {
      String dbFilePath = Configuration.getInstance().getProperty("DBFilepath", "acer-uti-gen.db");
      Files.deleteIfExists(Paths.get(dbFilePath));
    } catch (IOException e) {
      e.printStackTrace();
    }    
  }
  

  @BeforeEach
  void setup() {
    tradeBuilder = new TradeDetailsBuilder();
    //@formatter:off
    tradeBuilder.buyerCode("C0643778C.EU")
                .sellerCode("C06AG978W.EU")
                .contractType(ContractType.FORWARD)
                .commodity(Commodity.ELECTRICITY)
                .settlement(SettlementMethod.PHYSICAL)
                .tradeDate("2014-11-21")
                .price(53.5)
                .priceCurrency(Currency.EUR)
                .quantity(24.0)
                .quantityUnit(QuantityUnit.MTHERM_PER_D)
                .deliveryPoint("10YCB-EUROPEU--8")
                .startDate("2015-01-01")
                .endDate("2015-01-31");
    //@formatter:on
  }
  

  @Test
  @DisplayName("Electricity Forward MTherm - Normalizer")
  void testElectricityForwardNormalizer() {
    Configuration.getInstance().setProperty("NormalizerType", "Normalizer");
    TradeDetails trade = tradeBuilder.build();
    UtiGenerator generator = new UtiGenerator();
    assertEquals("Na5Mgwy9QBpng1atMLKzAihH9nIBHloSvo4JFQpR50001", generator.generateUTI(trade));
  }
  

  @Test
  @DisplayName("Electricity Forward MTherm")
  void testElectricityForward() {
    TradeDetails trade = tradeBuilder.build();
    UtiGenerator generator = new UtiGenerator();
    assertEquals("7RWCJiBbbYoGdtA4dskxQmyICoC1UBSAXDQZKmcjYX001", generator.generateUTI(trade));
  }
  
  
  @Test
  @DisplayName("Electricity No Price")
  void testElectricityNoPrice() {
    //@formatter:off
    TradeDetails trade = tradeBuilder.buyerCode("C0643778W.EU")
                                     .price(null)
                                     .priceCurrency(null)
                                     .quantity(10.0)
                                     .quantityUnit(QuantityUnit.fromUnitName("MWh/h"))
                                     .build();
    //@formatter:on
    UtiGenerator generator = new UtiGenerator();
    assertEquals("v3Y9Z6BFbaatVrxFl59Z2a7sP33ukZCepB1ZcRIFgY001", generator.generateUTI(trade));
  }
  
  
  @Test
  @DisplayName("Electricity Swap")
  void testElectricitySwap() {
    //@formatter:off
    TradeDetails trade = tradeBuilder.buyerCode("C0643778G.EU")
        .contractType(ContractType.SWAP)
        .settlement(SettlementMethod.OPTIONAL)
        .priceCurrency(Currency.fromCurrencyName("GBP"))
        .quantity(10.0)
        .quantityUnit(QuantityUnit.MWH_PER_H)
        .build();
    //@formatter:on
    UtiGenerator generator = new UtiGenerator();
    assertEquals("bxMWc1FI0P4FaHR3Y8zigNUX5FepEsAg4dw8O1VmCJ001", generator.generateUTI(trade));
  }
  
  
  @Test
  @DisplayName("Electricity Forward GJ")
  void testElectricityForwardGJ() {
    //@formatter:off
    TradeDetails trade = tradeBuilder.buyerCode("C0643778W.EU")
                                     .priceCurrency(Currency.GBP)
                                     .quantityUnit(QuantityUnit.GJ_PER_D)
                                     .deliveryPoint("10YCB-EUROPEU--1")
                                     .build();
    //@formatter:on
    UtiGenerator generator = new UtiGenerator();
    assertEquals("2UXpjxSOurFBu4TyGByQ0j9v3EB4rhXGpUGayBrZM7001", generator.generateUTI(trade));
  }
  
  
  @Test
  @DisplayName("Natural Gas Progressive")
  void testGas() {
    //@formatter:off
    TradeDetails trade = tradeBuilder.buyerCode("C0643778W.EU")
                                     .contractType(ContractType.SWING)
                                     .commodity(Commodity.NATURAL_GAS)
                                     .quantity(24000.0)
                                     .quantityUnit(QuantityUnit.MWH_PER_H)
                                     .deliveryPoint("37Y701125MH0000I")
                                     .build();
    //@formatter:on
    UtiGenerator generator = new UtiGenerator();
    assertEquals("N5Aimz49T5jx5JusDngPR6yLQO5kN9THSjytl3mmZA001", generator.generateUTI(trade));
    
    //@formatter:off
    trade = tradeBuilder.buyerCode("C0643778W.EU")
                        .commodity(Commodity.NATURAL_GAS)
                        .quantity(24000.0)
                        .quantityUnit(QuantityUnit.fromUnitName("MWhPERh"))
                        .deliveryPoint("37Y701125MH0000I")
                        .build();
    //@formatter:on

    assertEquals("N5Aimz49T5jx5JusDngPR6yLQO5kN9THSjytl3mmZA002", generator.generateUTI(trade));

  }
  
}
