package reporting.utils.enums;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum QuantityUnit {

  //@formatter:off
  KW("MW", 0.001), 
  KWH_PER_H("MW", 0.001),
  KWH_PER_D("MW", 0.001, 1/24d), 
  MW("MW"), 
  MWH_PER_H("MW"), 
  MWH("MW"), 
  MWH_PER_D("MW", 1.0, 1/24d), 
  GW("MW", 1000.0), 
  GWH_PER_H("MW", 1000.0), 
  GWH_PER_D("MW", 1000.0, 1/24d), 
  THERMS_PER_D("Therm/d"), 
  KTHERM_PER_D("Therm/d", 1000.0), 
  MTHERM_PER_D("Therm/d", 1000000.0), 
  CM_PER_D("cm/d"), 
  MCM_PER_D("cm/d", 1000000.0), 
  TCM_PER_D("cm/d", 1000.0), 
  BTU_PER_D("Btu/d"), 
  MMBTU_PER_D("Btu/d", 1000000.0), 
  MJ_PER_D("MJ/d"), 
  MJ_100_PER_D("MJ/d", 100.0), 
  MMJ_PER_D("MJ/d", 1000000.0), 
  GJ_PER_D("MJ/d", 1000.0);
  //@formatter:on

  private String standardUnitName;
  private BigDecimal multiplier;
  private BigDecimal flowMultiplier;

  private QuantityUnit(String standardUnitName) {
    this.standardUnitName = standardUnitName;
    this.multiplier = BigDecimal.ONE;
    this.flowMultiplier = BigDecimal.ONE;
  }

  private QuantityUnit(String standardUnitName, double multiplier) {
    this.standardUnitName = standardUnitName;
    this.multiplier = BigDecimal.valueOf(multiplier).setScale(12, RoundingMode.HALF_UP);
    this.flowMultiplier = BigDecimal.ONE;
  }

  private QuantityUnit(String standardUnitName, double multiplier, double flowMultiplier) {
    this.standardUnitName = standardUnitName;
    this.multiplier = BigDecimal.valueOf(multiplier).setScale(12, RoundingMode.HALF_UP);
    this.flowMultiplier = BigDecimal.valueOf(flowMultiplier).setScale(12, RoundingMode.HALF_UP);
  }
  
  public String getStandardUnitName() {
    return standardUnitName;
  }

  public BigDecimal getMultiplier() {
    return multiplier;
  }

  public BigDecimal getFlowMultiplier() {
    return flowMultiplier;
  }
  
  /**
   * Converts String unit name into Enum. Example inputs: MWh, MWhPerDay, Therm, ThermPerDay, Therm/d, KWh/d
   * 
   * @param unitName
   * @return
   */
  public static QuantityUnit fromUnitName(String unitName) {
    if (unitName == null) {
      throw new IllegalArgumentException("UnitName is not provided");
    }

    String lookUpName = unitName.toUpperCase();

    if (lookUpName.contains("PER") && !lookUpName.contains("_")) {
      // Handle cases like ThermPerDay
      lookUpName = lookUpName.replace("PER", "_PER_");

    } else if (lookUpName.contains("/")) {
      // Handle cases like Therm/d
      lookUpName = lookUpName.replace("/", "_PER_");
    }
    
    QuantityUnit selectedUnit = null;
    for (QuantityUnit unit : QuantityUnit.values()) {
      if (unit.name().equals(lookUpName)) {
        selectedUnit = unit;
        break;
      }
    }
    if (selectedUnit == null) {
      throw new IllegalArgumentException("No QuantityUnit found for '" + unitName + "'");
    }

    return selectedUnit;
  }

}
