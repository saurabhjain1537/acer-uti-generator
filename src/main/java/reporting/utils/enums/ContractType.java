package reporting.utils.enums;

public enum ContractType {
  FORWARD("FW"),
  OPTION("OP"),
  OPTION_FORWARD("OP_FW"),
  OPTION_SWAP("OP_SW"),
  SPREAD("SP"),
  SWAP("SW"),
  OPTION_SPREAD("OP_SP"),
  SWING("SWG");

  private String contractTypeValue;

  private ContractType(String contractTypeValue) {
    this.contractTypeValue = contractTypeValue;
  }

  public String getContractTypeValue() {
    return contractTypeValue;
  }

}
