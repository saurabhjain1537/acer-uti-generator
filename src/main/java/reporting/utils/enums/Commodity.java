package reporting.utils.enums;

public enum Commodity {
  ELECTRICITY("EL"), 
  NATURAL_GAS("NG");

  private String commodityValue;

  private Commodity(String commodityValue) {
    this.commodityValue = commodityValue;
  }

  public String getCommodityValue() {
    return commodityValue;
  }

}
