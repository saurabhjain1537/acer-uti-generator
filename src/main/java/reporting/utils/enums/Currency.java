package reporting.utils.enums;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum Currency {

  //@formatter:off
  EUR, 
  EUX("EUR", 0.01), 
  GBP, 
  GBX("GBP", 0.01), 
  HRK, 
  HUF, 
  ISK, 
  NOK, 
  PCT, 
  PLN, 
  RON, 
  SEK, 
  USD;
  //@formatter:on

  private String standardCurrencyName;
  private BigDecimal multiplier;

  private Currency() {
    this.standardCurrencyName = this.name();
    this.multiplier = BigDecimal.ONE;
  }

  private Currency(String standardCurrencyName, double multiplier) {
    this.standardCurrencyName = standardCurrencyName;
    this.multiplier = BigDecimal.valueOf(multiplier).setScale(10, RoundingMode.HALF_UP);
  }

  public String getStandardCurrencyName() {
    return standardCurrencyName;
  }

  public BigDecimal getMultiplier() {
    return multiplier;
  }

  public static Currency fromCurrencyName(String currencyName) {
    if (currencyName == null) {
      throw new IllegalArgumentException("CurrencyName is not provided");
    }
    Currency selectedCurrency = null;
    for (Currency currency : Currency.values()) {
      if (currency.name().equals(currencyName.toUpperCase())) {
        selectedCurrency = currency;
        break;
      }
    }
    if (selectedCurrency == null) {
      throw new IllegalArgumentException("No Currency found for '" + currencyName + "'");
    }
    return selectedCurrency;
  }

}
