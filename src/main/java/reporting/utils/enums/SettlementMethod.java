package reporting.utils.enums;

public enum SettlementMethod {
  PHYSICAL("P"),
  CASH("C"),
  OPTIONAL("O");

  private String settlementName;

  private SettlementMethod(String settlementName) {
    this.settlementName = settlementName;
  }

  public String getSettlementName() {
    return settlementName;
  }
  
}
