package reporting.utils.uti;

public class UtiGeneratorException extends RuntimeException {

  private static final long serialVersionUID = 7336781346574304963L;

  public UtiGeneratorException(String message, Throwable cause) {
    super(message, cause);
  }

  public UtiGeneratorException(String message) {
    super(message);
  }


}
