package reporting.utils.uti;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UtiGenerator {

  private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

  public String generateUTI(TradeDetails trade) {
    String normalizedAttributes = prepareConcatenatedValues(trade);
    logger.info("Concatenated Values after Normalization: {}", normalizedAttributes);
    String hashValue = prepareHash(normalizedAttributes);
    String dbFilePath = Configuration.getInstance().getProperty("DBFilepath", "acer-uti-gen.db");
    logger.debug("Database file path: {}", dbFilePath);
    String progressNumber = "";
    try (var hashDao = new UtiHashDao(dbFilePath)) {
      progressNumber = String.format("%03d",hashDao.incrementCount(hashValue));
    } catch (Exception e) {
      throw new UtiGeneratorException("Failed to increment hash '" + hashValue + "' count ", e);
    }
    return hashValue + progressNumber;
  }

  private String prepareConcatenatedValues(TradeDetails trade) {
    StringBuilder tradeAttributes = new StringBuilder();
    String normalizerType = Configuration.getInstance().getProperty("NormalizerType", "Normalizer");
    Normalizer normalizer = null;
    if ("AcerNormalizer".equalsIgnoreCase(normalizerType)) {
      normalizer = new AcerNormalizer(trade);
    } else {
      normalizer = new Normalizer(trade);
    }

    //@formatter:off
    tradeAttributes.append(trade.getBuyerCode())
                   .append(trade.getSellerCode())
                   .append(normalizer.getContractType())
                   .append(trade.getCommodity().getCommodityValue())
                   .append(normalizer.getSettlementMethod())
                   .append(trade.getTradeDate())
                   .append(normalizer.getPrice())
                   .append(normalizer.getPriceCurrency())
                   .append(normalizer.getQuantity())
                   .append(normalizer.getQuantityUnit())
                   .append(trade.getDeliveryPoint())
                   .append(trade.getStartDate())
                   .append(trade.getEndDate());
    //@formatter:on

    return tradeAttributes.toString();
  }

  private String prepareHash(String concatenatedString) {
    byte[] utf8Bytes = concatenatedString.getBytes(StandardCharsets.UTF_8);
    String hashValue = "";
    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      byte[] hash = md.digest(utf8Bytes);
      hashValue = Base64.encodeBase64String(hash);
    } catch (NoSuchAlgorithmException e) {
      throw new UtiGeneratorException("Message Digest instance is not created", e);
    }
    return hashValue.replace("+", "A").replace("/", "B").replace("=", "C").replace("-", "D").substring(0, 42);
  }

}
