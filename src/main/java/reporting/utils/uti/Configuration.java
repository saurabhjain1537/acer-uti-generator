package reporting.utils.uti;

import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configuration {

  private final Properties configProp = new Properties();
  private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
  private static Configuration instance;
  private static String propsFileName;

  private Configuration(String propertiesFile) {

    try (InputStream inStream = this.getClass().getClassLoader().getResourceAsStream(propertiesFile)) {

      if (inStream == null) {
        logger.info("File {} doesn't exist in classpath", propertiesFile);
      } else {
        logger.info("Reading all properites from file {}", propertiesFile);
        configProp.load(inStream);
      }
    } catch (Exception ex) {
      throw new UtiGeneratorException("Unable to read from properties file " + propertiesFile, ex);
    }
  }

  public static Configuration getInstance(String propertiesFile) {
    if (instance == null || !propsFileName.equals(propertiesFile)) {
      propsFileName = propertiesFile;
      instance = new Configuration(propertiesFile);
    }
    return instance;
  }

  public static Configuration getInstance() {
    return getInstance("uti-generator.properties");
  }

  public String getProperty(String key, String defaultValue) {
    return configProp.getProperty(key, defaultValue);
  }

  void setProperty(String key, String value) {
    configProp.setProperty(key, value);
  }
  
}
