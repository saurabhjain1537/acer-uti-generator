package reporting.utils.uti;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * The class extends Normalizer to match the normalized values of ACER provided UTI Generator.
 * ACER provided UTI Generator behavior:
 * 1. Always returns Quantity 1.0
 * 2. Divides Price by Quantity
 */
public class AcerNormalizer extends Normalizer {

  public AcerNormalizer(TradeDetails trade) {
    super(trade);
  }

  @Override
  public String getPrice() {

    BigDecimal normalizedPrice = getNormalizedPrice();
    BigDecimal quantity = BigDecimal.valueOf(trade.getQuantity());
    return normalizedPrice.divide(quantity, 5, RoundingMode.HALF_UP).toPlainString();
  }
  
  
  @Override
  public String getQuantity() {
    return BigDecimal.ONE.setScale(10, RoundingMode.HALF_UP).toPlainString();
  }

}
