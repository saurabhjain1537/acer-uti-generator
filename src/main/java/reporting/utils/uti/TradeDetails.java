package reporting.utils.uti;

import reporting.utils.enums.Commodity;
import reporting.utils.enums.ContractType;
import reporting.utils.enums.Currency;
import reporting.utils.enums.QuantityUnit;
import reporting.utils.enums.SettlementMethod;

public class TradeDetails {

  private final String buyerCode;
  private final String sellerCode;
  private final ContractType contractType;
  private final Commodity commodity;
  private final SettlementMethod settlement;
  private final String tradeDate;
  private final Double price;
  private final Currency priceCurrency;
  private final Double quantity;
  private final QuantityUnit quantityUnit;
  private final String deliveryPoint;
  private final String startDate;
  private final String endDate;

  public TradeDetails(TradeDetailsBuilder tradeDetailsBuilder) {
    if(tradeDetailsBuilder.buyerCode == null || tradeDetailsBuilder.buyerCode.isBlank()) {
      throw new IllegalArgumentException("buyerCode is missing.");
    }
    this.buyerCode = tradeDetailsBuilder.buyerCode;

    if(tradeDetailsBuilder.sellerCode == null || tradeDetailsBuilder.sellerCode.isBlank()) {
      throw new IllegalArgumentException("sellerCode is missing.");
    }
    this.sellerCode = tradeDetailsBuilder.sellerCode;

    if(tradeDetailsBuilder.contractType == null) {
      throw new IllegalArgumentException("contractType is missing.");
    }
    this.contractType = tradeDetailsBuilder.contractType;

    if(tradeDetailsBuilder.commodity == null) {
      throw new IllegalArgumentException("commodity is missing.");
    }
    this.commodity = tradeDetailsBuilder.commodity;

    if(tradeDetailsBuilder.settlement == null) {
      throw new IllegalArgumentException("settlement is missing.");
    }
    this.settlement = tradeDetailsBuilder.settlement;

    validateDate("tradeDate", tradeDetailsBuilder.tradeDate);
    this.tradeDate = tradeDetailsBuilder.tradeDate;

    this.price = tradeDetailsBuilder.price;
    this.priceCurrency = tradeDetailsBuilder.priceCurrency;
    this.quantity = tradeDetailsBuilder.quantity;
    this.quantityUnit = tradeDetailsBuilder.quantityUnit;

    if(tradeDetailsBuilder.deliveryPoint == null || tradeDetailsBuilder.deliveryPoint.isBlank()) {
      throw new IllegalArgumentException("deliveryPoint is missing.");
    }
    this.deliveryPoint = tradeDetailsBuilder.deliveryPoint;

    validateDate("startDate", tradeDetailsBuilder.startDate);
    this.startDate = tradeDetailsBuilder.startDate;

    validateDate("endDate", tradeDetailsBuilder.endDate);
    this.endDate = tradeDetailsBuilder.endDate;
  }

  private static void validateDate(String fieldName, String dateValue) {
    if(dateValue == null || dateValue.isBlank()) {
      throw new IllegalArgumentException(fieldName + " is missing.");
    }
    if(!dateValue.matches("^\\d{4}-\\d{2}-\\d{2}$")) {
      throw new IllegalArgumentException(fieldName + " is invalid. Valid format: YYYY-MM-DD.");
    }
  }

  public String getBuyerCode() {
    return buyerCode;
  }

  public String getSellerCode() {
    return sellerCode;
  }

  public ContractType getContractType() {
    return contractType;
  }

  public Commodity getCommodity() {
    return commodity;
  }

  public SettlementMethod getSettlement() {
    return settlement;
  }

  public String getTradeDate() {
    return tradeDate;
  }

  public Double getPrice() {
    return price;
  }

  public Currency getPriceCurrency() {
    return priceCurrency;
  }

  public Double getQuantity() {
    return quantity;
  }

  public QuantityUnit getQuantityUnit() {
    return quantityUnit;
  }

  public String getDeliveryPoint() {
    return deliveryPoint;
  }

  public String getStartDate() {
    return startDate;
  }

  public String getEndDate() {
    return endDate;
  }


  public static class TradeDetailsBuilder {
    private String buyerCode;
    private String sellerCode;
    private ContractType contractType;
    private Commodity commodity;
    private SettlementMethod settlement;
    private String tradeDate;
    private Double price;
    private Currency priceCurrency;
    private Double quantity;
    private QuantityUnit quantityUnit;
    private String deliveryPoint;
    private String startDate;
    private String endDate;

    public TradeDetailsBuilder buyerCode(String buyerCode) {
      this.buyerCode = buyerCode;
      return this;
    }

    public TradeDetailsBuilder sellerCode(String sellerCode) {
      this.sellerCode = sellerCode;
      return this;
    }

    public TradeDetailsBuilder contractType(ContractType contractType) {
      this.contractType = contractType;
      return this;
    }

    public TradeDetailsBuilder commodity(Commodity commodity) {
      this.commodity = commodity;
      return this;
    }

    public TradeDetailsBuilder settlement(SettlementMethod settlement) {
      this.settlement = settlement;
      return this;
    }

    public TradeDetailsBuilder tradeDate(String tradeDate) {
      this.tradeDate = tradeDate;
      return this;
    }

    public TradeDetailsBuilder price(Double price) {
      this.price = price;
      return this;
    }

    public TradeDetailsBuilder priceCurrency(Currency priceCurrency) {
      this.priceCurrency = priceCurrency;
      return this;
    }
    
    public TradeDetailsBuilder quantity(Double quantity) {
      this.quantity = quantity;
      return this;
    }

    public TradeDetailsBuilder quantityUnit(QuantityUnit quantityUnit) {
      this.quantityUnit = quantityUnit;
      return this;
    }

    public TradeDetailsBuilder deliveryPoint(String deliveryPoint) {
      this.deliveryPoint = deliveryPoint;
      return this;
    }

    public TradeDetailsBuilder startDate(String startDate) {
      this.startDate = startDate;
      return this;
    }

    public TradeDetailsBuilder endDate(String endDate) {
      this.endDate = endDate;
      return this;
    }

    public TradeDetails build() {
      return new TradeDetails(this);
    }

  }
}
