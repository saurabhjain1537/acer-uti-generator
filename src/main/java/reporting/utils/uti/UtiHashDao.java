package reporting.utils.uti;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UtiHashDao implements AutoCloseable {

  private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
  private final Connection connection;
  private final String tableName;
  private final String dbFilePath;

  public UtiHashDao() {
    this.dbFilePath = "acer-uti-gen.db";
    this.tableName = "uti_hash";
    this.connection = connect();
    createTable();
  }

  UtiHashDao(String dbFilePath) {
    this.dbFilePath = dbFilePath;
    this.tableName = "uti_hash";
    this.connection = connect();
    createTable();
  }
  
  public int incrementCount(String hashValue) {

    int count = 0;

    try {
      insertHash(hashValue);
      count = getCount(hashValue);
      connection.commit();

    } catch (Exception e) {
      try {
        connection.rollback();
      } catch (SQLException e1) {
        logger.warn("Ignoring Error in DB transaction rollback. ", e1);
      }
      throw new UtiGeneratorException("Failed to increment hash '" + hashValue + "' count ", e);
    }

    return count;
  }

  private int getCount(String hashValue) {

    StringBuilder countHashSql = new StringBuilder();
    countHashSql.append("SELECT count(1) FROM ").append(tableName).append(" WHERE hash = ? ");

    int count = 0;
    try (var stmt = connection.prepareStatement(countHashSql.toString())) {
      stmt.setString(1, hashValue);
      logger.info("Selecting from DB : {} ", countHashSql);
      var queryResult = stmt.executeQuery();

      if (queryResult.next()) {
        count = queryResult.getInt(1);
      }

    } catch (SQLException e) {
      throw new UtiGeneratorException("Failed to select from table " + tableName, e);
    }

    return count;

  }

  private void insertHash(String hashValue) {

    StringBuilder inserRowSql = new StringBuilder();
    inserRowSql.append("INSERT INTO ").append(tableName).append(" (hash) VALUES(?) ");

    try (var stmt = connection.prepareStatement(inserRowSql.toString())) {
      stmt.setString(1, hashValue);
      logger.info("Inserting into DB : {} ", inserRowSql);
      stmt.executeUpdate();
      
    } catch (SQLException e) {
      throw new UtiGeneratorException("Failed to insert into table " + tableName, e);
    }

  }

  private Connection connect() {
    String url = "jdbc:sqlite:" + dbFilePath;
    Connection conn = null;
    try {
      conn = DriverManager.getConnection(url);
      conn.setAutoCommit(false);
    } catch (SQLException e) {
      throw new UtiGeneratorException("Failed to connect Sqlite in-memory.", e);
    }
    return conn;
  }

  private void createTable() {

    StringBuilder createTableSql = new StringBuilder();
    createTableSql.append("CREATE TABLE IF NOT EXISTS ").append(tableName).append(" ( ").append("hash text").append(");");

    try (Statement stmt = connection.createStatement()) {
      logger.info("Creating DB Table: {} ", createTableSql);
      stmt.execute(createTableSql.toString());
      
    } catch (SQLException e) {
      throw new UtiGeneratorException("Failed to create table " + tableName, e);
    }

  }

  @Override
  public void close() throws Exception {
    if(connection != null && connection.isValid(5)) {
      connection.close();
    }
  }

}
