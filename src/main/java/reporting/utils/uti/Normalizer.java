package reporting.utils.uti;

import java.math.BigDecimal;
import java.math.RoundingMode;
import reporting.utils.enums.ContractType;
import reporting.utils.enums.SettlementMethod;

public class Normalizer {

  protected final TradeDetails trade;

  
  public Normalizer(TradeDetails trade) {
    this.trade = trade;
  }

  
  public String getSettlementMethod() {
    SettlementMethod settlement = trade.getSettlement();
    if (SettlementMethod.OPTIONAL.equals(settlement)) {
      settlement = SettlementMethod.PHYSICAL;
    }
    return settlement.getSettlementName();
  }

  
  public String getContractType() {
    var settlement = trade.getSettlement();
    ContractType contractType = trade.getContractType();
    ContractType normalizedContractType = switch (contractType) {
      case SWAP: {
        if (SettlementMethod.CASH.equals(settlement)) {
          yield contractType;
        } else {
          yield ContractType.FORWARD;
        }
      }

      case SPREAD: {
        if (SettlementMethod.CASH.equals(settlement)) {
          yield ContractType.SWAP;
        } else {
          yield ContractType.FORWARD;
        }
      }

      case SWING: {
        if (SettlementMethod.PHYSICAL.equals(settlement)) {
          yield ContractType.FORWARD;
        } else {
          yield contractType;
        }
      }

      case OPTION_FORWARD, OPTION_SPREAD, OPTION_SWAP:
        yield ContractType.OPTION;

      default:
        yield contractType;
    };

    return normalizedContractType.getContractTypeValue();

  }

  
  public String getPriceCurrency() {
    var priceCurrency = trade.getPriceCurrency();
    return priceCurrency == null ? "" : priceCurrency.getStandardCurrencyName();
  }

  
  public String getPrice() {
    return getNormalizedPrice().toPlainString();
  }
  
  protected BigDecimal getNormalizedPrice() {
    var price = trade.getPrice();
    var priceCurrency = trade.getPriceCurrency();

    BigDecimal multiplier = BigDecimal.ONE;
    if (priceCurrency != null) {
      multiplier = priceCurrency.getMultiplier();
    }

    var unit = trade.getQuantityUnit();

    BigDecimal unitDivisor = BigDecimal.ONE;
    if (unit != null) {
      //Multiplier for Unit is Divisor for Price e.g. 5 EUR per KWH = 5000 EUR per MWH (i.e. divide by .001)
      unitDivisor = unit.getMultiplier();
    }

    BigDecimal normalizedPrice = BigDecimal.ZERO;
    if (price != null) {
      //@formatter:off
      normalizedPrice = BigDecimal.valueOf(price)
                                  .multiply(multiplier)
                                  .divide(unitDivisor)
                                  .setScale(5, RoundingMode.HALF_UP);
      //@formatter:on
    }

    return normalizedPrice;    
  }

  
  public String getQuantityUnit() {
    var unit = trade.getQuantityUnit();
    return unit == null ? "" : unit.getStandardUnitName();
  }


  public String getQuantity() {
    var quantity = trade.getQuantity();
    var unit = trade.getQuantityUnit();

    BigDecimal multiplier = BigDecimal.ONE;
    BigDecimal flowMultiplier = BigDecimal.ONE;
    if (unit != null) {
      multiplier = unit.getMultiplier();
      flowMultiplier = unit.getFlowMultiplier();
    }

    String normalizedQuantity = "";
    if (quantity != null) {
      //@formatter:off
      normalizedQuantity = BigDecimal.valueOf(quantity)
                                  .multiply(multiplier)
                                  .multiply(flowMultiplier)
                                  .setScale(10, RoundingMode.HALF_UP)
                                  .toPlainString();
      //@formatter:on
    }

    return normalizedQuantity;
  }
  
}
